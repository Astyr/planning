&include "types.4gl"
FUNCTION validateHoliday(usr)
    DEFINE usr t_user
    DEFINE i INT
    DEFINE tmpHoliday t_inProgressHolidayTmp
    DEFINE inProgressHolidays DYNAMIC ARRAY OF t_inProgressHoliday
--DEFINE recordInProgress DYNAMIC ARRAY OF t_inProgressHoliday
    DEFINE curRow INT
    DEFINE logComment LIKE log_activity.comment
    OPEN WINDOW w1 WITH FORM "confirmHoliday"

    DECLARE curs_list CURSOR FOR

    SELECT TYPE, date_begin, duration, date_end, COMMENT, balance_rt, balance_holiday, username, holiday.id AS id
    FROM USER, holiday
    WHERE user.id = holiday.id_user
    AND (user.id_chief = usr.id
    OR holiday.id_user = usr.id)
    AND holiday.status LIKE "Waiting"

    LET i = 0
                
    FOREACH curs_list INTO tmpHoliday.*
    LET i = i + 1
    LET inProgressHolidays[i].comment = tmpHoliday.comment
    LET inProgressHolidays[i].date_begin = tmpHoliday.date_begin
    LET inProgressHolidays[i].date_end = tmpHoliday.date_end
    LET inProgressHolidays[i].duration = tmpHoliday.duration
    LET inProgressHolidays[i].id = tmpHoliday.id
    LET inProgressHolidays[i].type = tmpHoliday.type
    LET inProgressHolidays[i].username = tmpHoliday.username
    
    IF tmpHoliday.type = "Holidays"
    THEN
        LET inProgressHolidays[i].balance_rt = tmpHoliday.balance_holiday / 2
    ELSE
    IF tmpHoliday.type = "Days off"
    THEN
        LET inProgressHolidays[i].balance_rt = tmpHoliday.balance_rt / 2
    ELSE
        LET inProgressHolidays[i].balance_rt = NULL
    END IF
    END IF
    
    END FOREACH

   

    
    DISPLAY ARRAY inProgressHolidays TO recordInProgress.* ATTRIBUTE (UNBUFFERED)
        BEFORE ROW
            --DISPLAY DIALOG.getCurrentRow("recordInProgress")
            
        ON ACTION buttonDeny
            LET curRow = DIALOG.getCurrentRow("recordinprogress")
            UPDATE holiday
            SET status="Refused"
            WHERE holiday.id = inProgressHolidays[curRow].id
           -- CALL DIALOG.DeleteRow("recordinprogress", curRow)
            CALL inProgressHolidays.deleteElement(curRow)
            --DISPLAY inProgressHolidays[curRow].*

            LET logComment = "Deny holiday to " || inProgressHolidays[curRow].username
            || " from " ||  inProgressHolidays[curRow].date_begin || " to " || inProgressHolidays[curRow].date_end
            
            INSERT INTO log_activity
            (id_user, COMMENT, TYPE)
            VALUES
            (usr.id, logComment, "2")
        ON ACTION buttonAccept
            LET curRow = DIALOG.getCurrentRow("recordinprogress")
            UPDATE holiday
            SET status="Accepted"
            WHERE holiday.id = inProgressHolidays[curRow].id
           -- CALL DIALOG.DeleteRow("recordinprogress", curRow)
            CALL acceptHoliday(inProgressHolidays[curRow].*)
            CALL inProgressHolidays.deleteElement(curRow) 

            LET logComment = "Accept holiday to " || inProgressHolidays[curRow].username
            || " from " ||  inProgressHolidays[curRow].date_begin || " to " || inProgressHolidays[curRow].date_end
            
            INSERT INTO log_activity
            (id_user, COMMENT, TYPE)
            VALUES
            (usr.id, logComment, "2")
    END DISPLAY

   -- MENU
    --    
    -- ON ACTION buttonDeny
     --       DISPLAY recordInProgress.balance_rt
      --      EXIT MENU
      --  ON ACTION buttonAccept
     --       DISPLAY "todo"
     --       EXIT MENU
    --END MENU
    CLOSE WINDOW w1
END FUNCTION

FUNCTION acceptHoliday(holiday)
    DEFINE holiday t_inProgressHoliday

    CASE holiday.type
        WHEN "Holidays"
            UPDATE USER SET balance_holiday = balance_holiday - holiday.duration WHERE username = holiday.username
        WHEN "Days off"
            UPDATE USER SET balance_rt = balance_rt - holiday.duration WHERE username = holiday.username
    END CASE    

END FUNCTION