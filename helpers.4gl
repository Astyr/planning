&include "types.4gl"

FUNCTION getNode(ntype, nname)
    DEFINE ntype, nname STRING
    DEFINE win ui.Window
    DEFINE f ui.Form
    DEFINE n om.DomNode

    LET win = ui.Window.getCurrent()
    LET f = win.getForm()
    LET n = f.findNode(ntype, nname)

    RETURN n

END FUNCTION

FUNCTION getFormField(nname)
   DEFINE nname STRING
   DEFINE n om.DomNode

    LET n = getNode("FormField", nname)
    LET n = n.getFirstChild()

    RETURN n

END FUNCTION