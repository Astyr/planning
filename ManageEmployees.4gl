&include "types.4gl"

TYPE t_manageEnd RECORD
    e_firstname LIKE user.firstname,
    e_lastname LIKE user.lastname,
    e_username LIKE user.username,
    e_holiday LIKE user.balance_holiday,
    e_rt LIKE user.balance_rt,
    id_chief STRING,
    isAdmin STRING
END RECORD

FUNCTION refreshUsers(usr, manage)
    DEFINE usr t_user
    DEFINE i INT
    DEFINE tmpManage t_manage
    DEFINE manage DYNAMIC ARRAY OF t_manageEnd

    DECLARE curs_list CURSOR FOR
     SELECT firstname AS efn, lastname AS eln, username, balance_holiday, balance_rt / 2, id_chief, is_admin
    FROM USER

   
    LET i = 0
                
    FOREACH curs_list INTO tmpManage.*
    LET i = i + 1
      LET manage[i].e_firstname = tmpManage.e_firstname
      LET manage[i].e_lastname = tmpManage.e_lastname 
     LET manage[i].e_username =tmpManage.e_username
     LET manage[i].e_holiday = tmpManage.e_holiday
     LET manage[i].e_rt = tmpManage.e_rt
     IF tmpManage.id_chief = usr.id
        THEN
        LET manage[i].id_chief ="YES"
     ELSE
        LET manage[i].id_chief ="NO"
     END IF

     
     IF tmpManage.isAdmin = TRUE
     THEN
     LET manage[i].isAdmin = "YES"
     ELSE
     LET manage[i].isAdmin = "NO"
     END IF
    END FOREACH
END FUNCTION

FUNCTION manage(usr)
    DEFINE usr t_user
    
    DEFINE manage DYNAMIC ARRAY OF t_manageEnd
    DEFINE curRow INT
    DEFINE idToDelete LIKE user.id
    DEFINE answer STRING
    DEFINE spinrt INT
    DEFINE spinholiday INT
    DEFINE n om.DomNode
    DEFINE commentLog LIKE log_activity.comment
    
    OPEN WINDOW w1 WITH FORM "manageemployees"

    CALL refreshUsers(usr.*, manage)

    LET n = getNode("Button", "validatedays")

    DISPLAY ARRAY manage TO record1.* ATTRIBUTE (UNBUFFERED)

        BEFORE ROW
         LET curRow = DIALOG.getCurrentRow("record1")
            DISPLAY manage[curRow].e_rt TO spinrt
            DISPLAY manage[curRow].e_holiday TO spinholiday
        ON ACTION validatedays 
            INPUT BY NAME spinrt, spinholiday ATTRIBUTE (UNBUFFERED)
                BEFORE INPUT
                    CALL n.setAttribute("text", "Validate")
                    
                ON ACTION validatedays 
                IF  spinholiday IS NULL
                THEN
                    ERROR "balance holiday is null"
                    EXIT INPUT
                END IF
                IF  spinrt IS NULL
                THEN
                    ERROR "balance rt is null"
                    EXIT INPUT
                END IF
                LET commentLog = manage[curRow].e_username || "'s holiday balance has been set from:"
                || manage[curRow].e_holiday || " to:" || spinholiday || " and days off balance has been set from:"
                || manage[curRow].e_rt || " to:" || spinrt 
                INSERT INTO log_activity
                (id_user, COMMENT, TYPE)
                VALUES
                (usr.id, commentLog, "3")
                LET manage[curRow].e_holiday = spinholiday
                LET manage[curRow].e_rt = spinrt
                UPDATE USER
                SET balance_rt=(spinrt * 2),
                balance_holiday=spinholiday
                WHERE username= manage[curRow].e_username
                EXIT INPUT
            END INPUT

            CALL n.setAttribute("text", "Modify balances")
        ON ACTION promote
            UPDATE user
            SET is_admin=true
            WHERE user.username = manage[curRow].e_username
            LET manage[curRow].isAdmin = "YES"

            LET commentLog = manage[curRow].e_username || " is now administrator"

            INSERT INTO log_activity
            (id_user, COMMENT, TYPE)
            VALUES
            (usr.id, commentLog, "3")
        ON ACTION demote
            UPDATE user
            SET is_admin=false
            WHERE user.username = manage[curRow].e_username
            LET manage[curRow].isAdmin = "NO"
            
            LET commentLog = manage[curRow].e_username || " is no more administrator"

            INSERT INTO log_activity
            (id_user, COMMENT, TYPE)
            VALUES
            (usr.id, commentLog, "3")
        ON ACTION myemployee
                UPDATE user
                SET id_chief = usr.id
                WHERE user.username = manage[curRow].e_username
                LET manage[curRow].id_chief = "YES"  

                LET commentLog = " set " || manage[curRow].e_username || " as his/her employee"

                INSERT INTO log_activity
                (id_user, COMMENT, TYPE)
                VALUES
                (usr.id, commentLog, "3")

                
        ON ACTION adduser
            LET answer = addUser(usr.*)
            IF answer = "Yes" THEN
                CALL FGL_WINMESSAGE( "Info", "User added !", "information")
                CALL refreshUsers(usr.*, manage)
            END IF

        ON ACTION modifyuser
            CALL modifyuser(usr.*, manage[curRow].*)
            CALL refreshUsers(usr.*, manage)
        ON ACTION deleteuser

            LET answer = FGL_WINBUTTON( "Warning", "Are you sure ?",
            "Lynx", "Yes|No", "question", 0)
            IF answer = "Yes" THEN
            LET curRow = DIALOG.getCurrentRow("record1")

            SELECT id INTO idToDelete FROM USER
            WHERE user.username = manage[curRow].e_username

            DELETE FROM user
            WHERE user.username = manage[curRow].e_username

            DELETE FROM holiday
            WHERE holiday.id_user = idToDelete
            CALL manage.deleteElement(curRow)
            CALL FGL_WINMESSAGE( "Info", "User deleted !", "information")

            LET commentLog = " delete " || manage[curRow].e_username

            INSERT INTO log_activity
            (id_user, COMMENT, TYPE)
            VALUES
            (usr.id, commentLog, "3")
        
            END IF
    END DISPLAY
    
    CLOSE WINDOW w1
END FUNCTION