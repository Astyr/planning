&include "types.4gl"

SCHEMA planningdb

MAIN
    CONNECT TO "planningdb"
    
    CALL ui.Interface.loadActionDefaults("mainAD")
    CALL ui.Interface.loadStyles("mainST")

    CLOSE WINDOW SCREEN

    CALL prepareRequests()
    CALL showLogin()
    
END MAIN

FUNCTION showLogin()
    DEFINE connectInfo t_formConnect
    DEFINE usr t_user

    OPEN WINDOW wmain WITH FORM "connectionForm"

    INPUT BY NAME connectInfo.* ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED, ACCEPT=FALSE, CANCEL=FALSE)

        ON ACTION buttonLog
        CALL loginConnect(connectInfo.editusername, connectInfo.editpassword) RETURNING usr.*
            IF usr.id != 0 THEN
                CALL showUserLanding(usr.*)
            ELSE
                ERROR "Bad login / password !"
            END IF

        ON ACTION CLOSE
            EXIT INPUT
    END INPUT
EXIT PROGRAM
END FUNCTION