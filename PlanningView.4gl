&include "types.4gl"

{DEFINE currentMonth INT
DEFINE currentMonthChar CHAR(24)
DEFINE currentMonthName STRING
DEFINE currentYear INT
DEFINE requestedDate CHAR(20)
DEFINE requestedTmpDate CHAR(20)
DEFINE att DYNAMIC ARRAY OF t_userPlanningFormated}

FUNCTION showPlanning(usr)
DEFINE usr t_user
{DEFINE arrayPlanningTable DYNAMIC ARRAY OF t_userPlanningFormated
DEFINE userOfPlanningFormated t_userPlanningFormated
DEFINE userOfPlanning t_userPlanning
DEFINE i INT}


# HOLIDAY CLASS
TYPE t_fillHoliday RECORD
    startH VARCHAR(100),
    endH VARCHAR(100),
    commentH LIKE holiday.comment,
    id_userH LIKE holiday.id_user
END RECORD

TYPE t_sendHoliday RECORD
    startH STRING,
    endH STRING,
    commentH STRING
END RECORD

TYPE t_infoUsr RECORD
    firstN LIKE user.firstname,
    lastN LIKE user.lastname
END RECORD

DEFINE isOk STRING
DEFINE holiday t_fillHoliday
DEFINE holidayall DYNAMIC ARRAY OF t_fillHoliday
DEFINE holidaylist t_sendHoliday
DEFINE info_usr t_infoUsr
DEFINE test STRING
DEFINE textRec STRING
DEFINE agendaTest STRING

OPEN WINDOW w WITH FORM "agendatest"
    
    PREPARE req FROM
    "SELECT DATE_FORMAT(date_begin, '%d-%m-%Y %H:%i'), 
    DATE_FORMAT(DATE_ADD(date_end, INTERVAL 1 DAY), '%d-%m-%Y %H:%i'),
    comment,
    id_user
    FROM holiday 
    WHERE status NOT LIKE 'Refused';"

   DECLARE holiday_list CURSOR FOR req

    FOREACH holiday_list INTO holiday.*

    SELECT firstname, lastname
    INTO info_usr.*
    FROM USER
    WHERE holiday.id_userH = id
    
    LET holidaylist.startH = holiday.startH
    LET holidaylist.endH = holiday.endH
    LET holidaylist.commentH = info_usr.firstN || " " || info_usr.lastN || " - " || holiday.commentH
        
    CALL ui.Interface.frontCall("webcomponent","call",
        ["formonly.agendatest","addfrom4glcopy", holidaylist.startH, holidaylist.endH, holidaylist.commentH],[isOk])

    END FOREACH

    INPUT BY NAME agendaTest ATTRIBUTES (UNBUFFERED, WITHOUT DEFAULTS)
    ON ACTION componentclick

        LET test = agendaTest
        CALL ui.Interface.frontCall("webcomponent","call",
            ["formonly.agendatest","gettextwithid",test],[textRec])
        CALL FGL_WINMESSAGE( "Missing reason", textRec, "question")

    ON ACTION CLOSE
       EXIT INPUT
    END INPUT


CLOSE WINDOW w




{OPEN WINDOW wplanning WITH FORM "planningview"

    DECLARE curs_list CURSOR FOR
    SELECT firstname, lastname, id FROM USER
    
    LET i = 0

    PREPARE sMonth FROM "SELECT month(now()), year(now())"
    EXECUTE sMonth INTO currentMonth, currentYear
    CALL changeMonth()
    
    LET i = 0
    
    FOREACH curs_list INTO userOfPlanning.*
        LET i = i + 1
        LET userOfPlanningFormated.fullname = userOfPlanning.firstname || " " || userOfPlanning.lastname
        LET userOfPlanningFormated.id = userOfPlanning.id
        LET arrayPlanningTable[i].* = userOfPlanningFormated.*
        END FOREACH

    CALL fillWithHolidays(arrayPlanningTable, i, att) RETURNING att
        
    DIALOG ATTRIBUTES(UNBUFFERED)
    DISPLAY ARRAY arrayPlanningTable TO PlanningTable.*
        BEFORE DISPLAY
            CALL DIALOG.setArrayAttributes("planningtable", att)

    
        ON ACTION CLOSE
            EXIT DIALOG
        ON ACTION prevMonth
            CALL changeToPrevMonth()
            CALL DIALOG.setArrayAttributes("planningtable", NULL)
            LET att = null
            CALL fillWithHolidays(arrayPlanningTable, i, att) RETURNING att
            CALL DIALOG.setArrayAttributes("planningtable", att)
        ON ACTION nextMonth
            CALL changeToNextMonth()
            CALL DIALOG.setArrayAttributes("planningtable", NULL)
            LET att = null
            CALL fillWithHolidays(arrayPlanningTable, i, att) RETURNING att
            CALL DIALOG.setArrayAttributes("planningtable", att)
        END DISPLAY
    END DIALOG

CLOSE WINDOW wplanning
END FUNCTION



FUNCTION fillDays(nbDay, firstDayName)
DEFINE i INT
DEFINE j INT
DEFINE nbDay INT
DEFINE firstDayName STRING
DEFINE dayName STRING

LET j = 1
CALL defineFirstDay(firstDayName) RETURNING i

WHILE j <= nbDay
    CALL returnDayName(i, j) RETURNING dayName
    CALL fillWithDayName(dayName, j)
    LET i = i + 1
    IF i = 8 THEN
        LET i = 1
    END IF
    LET j = j + 1
END WHILE

WHILE j < 32
    CALL fillWithDayName("", j)
    LET j = j + 1
END WHILE
END FUNCTION



FUNCTION returnDayName(i, j)
DEFINE i INT
DEFINE j INT
DEFINE dayName STRING

CASE i
     WHEN 1
       LET dayName = ("Mo " || j)
     WHEN 2
       LET dayName = ("Tu " || j)
     WHEN 3
       LET dayName = ("We " || j)
     WHEN 4
       LET dayName = ("Th " || j)
     WHEN 5
       LET dayName = ("Fr " || j)
     WHEN 6
       LET dayName = ("Sa " || j)
     WHEN 7
       LET dayName = ("Su " || j)
   END CASE

RETURN dayName
END FUNCTION



FUNCTION fillWithDayName(dayName, j)
DEFINE dayName STRING
DEFINE j INT
DEFINE n om.DomNode

LET n = getNode("TableColumn", "formonly.edit" || j + 1)
CALL n.setAttribute("text", dayName);
END FUNCTION



FUNCTION defineFirstDay(firstDayName)
DEFINE firstDayName STRING
DEFINE i INT

CASE firstDayName
     WHEN "Monday"
       LET i = 1
     WHEN "Tuesday"
       LET i = 2
     WHEN "Wednesday"
       LET i = 3
     WHEN "Thursday"
       LET i = 4
     WHEN "Friday"
       LET i = 5
     WHEN "Saturday"
       LET i = 6
     WHEN "Sunday"
       LET i =7
   END CASE

RETURN i
END FUNCTION




FUNCTION changeToPrevMonth()
LET currentMonth = currentMonth - 1
IF currentMonth = 0 THEN
    LET currentMonth = 12
    LET currentYear = currentYear - 1
END IF

CALL changeMonth()
END FUNCTION




FUNCTION changeToNextMonth()
LET currentMonth = currentMonth + 1
IF currentMonth = 13 THEN
    LET currentMonth = 1
    LET currentYear = currentYear + 1
END IF

CALL changeMonth()
END FUNCTION




FUNCTION changeMonth()
DEFINE nbMonth INT
DEFINE firstDayName CHAR(20)
DEFINE tmpMonth INT
DEFINE tmpYear INT

    LET tmpMonth = currentMonth + 1
    LET tmpYear = currentYear
    IF tmpMonth = 13 THEN
        LET tmpMonth = 1
        LET tmpYear = tmpYear + 1
    END IF
    LET requestedTmpDate = tmpYear || "-" || tmpMonth || "-01"
    
    PREPARE sNbDaysNext FROM "SELECT day(date_add(?, INTERVAL -day(?) + 0 DAY))"
    EXECUTE sNbDaysNext USING requestedTmpDate, requestedTmpDate INTO nbMonth

    LET requestedDate = currentYear || "-" || currentMonth || "-01"
   
PREPARE sFirstDayNextMonth FROM "SELECT dayname(date_add(?, INTERVAL -day(?) + 1 DAY))"
EXECUTE sFirstDayNextMonth USING requestedDate, requestedDate INTO firstDayName

CALL fillDays(nbMonth, firstDayName)

PREPARE sMonthNextName FROM "SELECT monthname(?)"
EXECUTE sMonthNextName USING requestedDate INTO currentMonthChar
LET currentMonthName = currentMonthChar
LET currentMonthName = currentMonthName.trim()
DISPLAY currentMonthName || " " || currentYear TO currenttime
END FUNCTION



FUNCTION fillWithHolidays(arrayPlanningTable, i, att)
DEFINE arrayPlanningTable DYNAMIC ARRAY OF t_userPlanningFormated
DEFINE att DYNAMIC ARRAY OF t_userPlanningFormated
DEFINE i INT
DEFINE holidayPlanning t_holiday
DEFINE j INT
DEFINE cDay INT
DEFINE tDate DATE
DEFINE nbMonth INT
DEFINE color STRING

    LET j = 1
    LET cDay = 1
    
    PREPARE NbDaysNext FROM "SELECT day(date_add(?, INTERVAL -day(?) + 0 DAY))"
    EXECUTE NbDaysNext USING requestedTmpDate, requestedTmpDate INTO nbMonth
    
    DECLARE holiday_list CURSOR FOR
    
    SELECT * FROM holiday
    WHERE status NOT LIKE 'Refused'
    AND holiday.date_end >= requestedDate
    AND holiday.date_begin <= requestedTmpDate

    FOREACH holiday_list INTO holidayPlanning.*
    WHILE holidayPlanning.id_user != arrayPlanningTable[j].id AND j < i
        LET j = j + 1
    END WHILE

    IF holidayPlanning.status = "Accepted" THEN
        LET color = "green reverse"
    END IF
    IF holidayPlanning.status = "Waiting" THEN
        LET color = "orange reverse"
    END IF
    
    WHILE cDay <= NbMonth
        LET tDate = MDY(currentMonth, cDay, currentYear)
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 1 THEN
            LET att[j].day1 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 2 THEN
            LET att[j].day2 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 3 THEN
            LET att[j].day3 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 4 THEN
            LET att[j].day4 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 5 THEN
            LET att[j].day5 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 6 THEN
            LET att[j].day6 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 7 THEN
            LET att[j].day7 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 8 THEN
            LET att[j].day8 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 9 THEN
            LET att[j].day9 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 10 THEN
            LET att[j].day10 = color
        END IF
            
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 11 THEN
            LET att[j].day11 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 12 THEN
            LET att[j].day12 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 13 THEN
            LET att[j].day13 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 14 THEN
            LET att[j].day14 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 15 THEN
            LET att[j].day15 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 16 THEN
            LET att[j].day16 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 17 THEN
            LET att[j].day17 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 18 THEN
            LET att[j].day18 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 19 THEN
            LET att[j].day19 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 20 THEN
            LET att[j].day20 = color
        END IF

        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 21 THEN
            LET att[j].day21 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 22 THEN
            LET att[j].day22 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 23 THEN
            LET att[j].day23 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 24 THEN
            LET att[j].day24 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 25 THEN
            LET att[j].day25 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 26 THEN
            LET att[j].day26 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 27 THEN
            LET att[j].day27 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 28 THEN
            LET att[j].day28 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 29 THEN
            LET att[j].day29 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 30 THEN
            LET att[j].day30 = color
        END IF
        IF tDate >= holidayPlanning.date_begin AND tDate <= holidayPlanning.date_end AND cDay = 31 THEN
            LET att[j].day31 = color
        END IF

        LET cDay = cDay + 1
        
    END WHILE
    LET cDay = 1
    LET j = 1
    END FOREACH

RETURN att}
END FUNCTION