&include "types.4gl"

TYPE t_manageEnd RECORD
    e_firstname LIKE user.firstname,
    e_lastname LIKE user.lastname,
    e_username LIKE user.username,
    e_holiday LIKE user.balance_holiday,
    e_rt LIKE user.balance_rt,
    id_chief STRING,
    isAdmin STRING
END RECORD

FUNCTION modifyUser(usr, manage)
    DEFINE usr t_user
    DEFINE modUser RECORD 
        firstname LIKE user.firstname,
        lastname LIKE user.lastname,
        is_admin LIKE user.is_admin,
        password LIKE user.password
        END RECORD 
    DEFINE logComment LIKE log_activity.comment
    DEFINE manage t_manageEnd
    OPEN WINDOW wadd WITH FORM "adduser"

    IF (manage.isAdmin = "YES") THEN
        LET modUser.is_admin = TRUE
    ELSE 
        LET modUser.is_admin = FALSE
    END IF

    LET modUser.firstname = manage.e_firstname
    LET modUser.lastname = manage.e_lastname
    
    INPUT BY NAME modUser.* ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED, ACCEPT=FALSE, CANCEL=FALSE)
    BEFORE INPUT
        DISPLAY manage.e_username TO username
        DISPLAY "Empty fields will not be changed (except admin)" TO infolabel
    ON ACTION submit

            IF (modUser.firstname is NULL) THEN
                LET modUser.firstname = manage.e_firstname
            END IF
            IF (modUser.lastname is NULL) THEN
                LET modUser.lastname = manage.e_lastname
            END IF
            IF (modUser.password is NULL) THEN
                UPDATE USER SET firstname=modUser.firstname, lastname=modUser.lastname, is_admin=modUser.is_admin
                WHERE user.username= manage.e_username
            ELSE
                UPDATE USER SET firstname=modUser.firstname, lastname=modUser.lastname, is_admin=modUser.is_admin, password=MD5(modUser.password)
                WHERE user.username= manage.e_username
            END IF
    
                LET logComment = manage.e_username || " has been modified"

                INSERT INTO log_activity
                (id_user, COMMENT, TYPE)
                VALUES
                (usr.id, logComment, "3")
                
                EXIT INPUT

    ON ACTION CLOSE
        EXIT INPUT
        
    END INPUT
    CLOSE WINDOW wadd
END FUNCTION