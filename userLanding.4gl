&include "types.4gl"

FUNCTION fillTableHoliday(usr, arrayHoliday, att)
    DEFINE usr t_user
    DEFINE i INT
    DEFINE cHoliday t_holiday
    DEFINE fillHoliday t_fillHoliday
    DEFINE att DYNAMIC ARRAY OF t_fillHoliday
    DEFINE arrayHoliday DYNAMIC ARRAY OF t_fillHoliday
 
    --DISPLAY usr.balance_holiday TO balanceHolidays
    --DISPLAY usr.balance_rt TO balanceDaysoff
    DISPLAY usr.firstname || " " || usr.lastname || " / " || usr.username TO username
    
    DECLARE curs_list CURSOR FOR

    SELECT * FROM holiday WHERE usr.id = holiday.id_user
    LET i = 0
        
    FOREACH curs_list INTO cHoliday.*
    LET i = i + 1
    LET fillHoliday.typeH = cHoliday.type
    LET fillHoliday.startH = cHoliday.date_begin
    LET fillHoliday.endH = cHoliday.date_end
    LET fillHoliday.commentH = cHoliday.comment
    LET fillHoliday.statusH = cHoliday.status
    LET arrayHoliday[i].* = fillHoliday.*

    IF fillHoliday.statusH = "Accepted" THEN
        LET att[i].statusH = "green reverse"
    END IF
    IF fillHoliday.statusH = "Waiting" THEN
        LET att[i].statusH = "orange reverse"
    END IF
    IF fillHoliday.statusH = "Passed" THEN
        LET att[i].statusH = "white reverse"
    END IF
    IF fillHoliday.statusH = "Refused" THEN
        LET att[i].statusH = "red reverse"
    END IF

    END FOREACH
END FUNCTION

FUNCTION showUserLanding(usr)
    DEFINE usr t_user
    

   
    DEFINE arrayHoliday DYNAMIC ARRAY OF t_fillHoliday
    DEFINE admin LIKE user.is_admin
    DEFINE att DYNAMIC ARRAY OF t_fillHoliday


    
    OPEN WINDOW ul WITH FORM 'userLanding'

    DISPLAY usr.balance_holiday / 2 TO balanceHolidays
    DISPLAY usr.balance_rt / 2 TO balanceDaysoff
    DISPLAY usr.firstname || " " || usr.lastname || " / " || usr.username TO username

    CALL fillTableHoliday(usr.*, arrayHoliday, att)

    SELECT user.is_admin
    INTO admin
    FROM user
    WHERE id = usr.id
   
    IF admin = FALSE THEN

           DIALOG ATTRIBUTES(UNBUFFERED)
           DISPLAY ARRAY arrayHoliday TO HolidayArray.*
            BEFORE DISPLAY
                CALL DIALOG.setArrayAttributes("holidayarray", att)

            ON ACTION registerHoliday
                CALL showRegisterHoliday(usr.*)
                CALL fillTableHoliday(usr.*, arrayHoliday, att)
            ON ACTION planning
                CALL showPlanning(usr.*)
            ON ACTION logout
                    EXIT DIALOG
            ON ACTION CLOSE
                    EXIT DIALOG
            END DISPLAY
        END DIALOG

      
    ELSE
           DIALOG ATTRIBUTES(UNBUFFERED)
           DISPLAY ARRAY arrayHoliday TO HolidayArray.*
            BEFORE DISPLAY
                CALL DIALOG.setArrayAttributes("holidayarray", att)


            ON ACTION planning
                CALL showPlanning(usr.*)
            ON ACTION manage
                CALL manage(usr.*)
                CALL fillTableHoliday(usr.*, arrayHoliday, att)
            ON ACTION registerHoliday
                CALL showRegisterHoliday(usr.*)
                CALL fillTableHoliday(usr.*, arrayHoliday, att)            
            ON ACTION confirmHoliday
                CALL validateHoliday(usr.*)
                CALL fillTableHoliday(usr.*, arrayHoliday, att)
            ON ACTION managePublicHolidays
                CALL showManagePublicHolidays(usr.*)
            ON ACTION buttonLog
                CALL showLog()
            ON ACTION logout
                EXIT DIALOG
            ON ACTION CLOSE
                EXIT DIALOG
            END DISPLAY
        END DIALOG
   END IF
    
    CLOSE WINDOW ul

END FUNCTION