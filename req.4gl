FUNCTION prepareRequests()

    CALL prepareNbWorkingDays()
    CALL prepareIsWorkingDay()

END FUNCTION

# (ONLY ?) MYSQL COMPATIBLE
FUNCTION prepareNbWorkingDays()

    PREPARE reqNbWorkingDays FROM "SELECT
    DATEDIFF(?, ?) + 1
    -
    ((FLOOR(DATEDIFF(?, ?) / 7)
    +
    IF(week(DATE_ADD(?, INTERVAL (DATEDIFF(?, ?) % 7) DAY)) != week(?), 1, 0)) * 2
    +
    (SELECT COUNT(*) FROM public_holidays
        WHERE dayofweek(date_holiday) NOT IN(1, 7)
        AND date_holiday >= ? 
        AND date_holiday <= ?))"

END FUNCTION

FUNCTION execNbWorkingDays(begin_date, end_date)
    DEFINE begin_date, end_date DATE
    DEFINE nb INT

    EXECUTE reqNbWorkingDays USING end_date, begin_date, end_date, begin_date, begin_date, end_date, begin_date, begin_date, begin_date, end_date
    INTO nb

    RETURN nb

END FUNCTION

FUNCTION prepareIsWorkingDay()

    PREPARE reqIsWorkingDay FROM "SELECT IF(
    (SELECT COUNT(*) FROM public_holidays WHERE date_holiday = ?) != 0 
    OR
    dayofweek(?) IN(1, 7)
    , FALSE
    , TRUE)"

END FUNCTION

FUNCTION execIsWorkingDay(isWorkingDate)
    DEFINE isWorkingDate DATE
    DEFINE isWorkingDay BOOLEAN

    EXECUTE reqIsWorkingDay USING isWorkingDate, isWorkingDate
    INTO isWorkingDay

    RETURN isWorkingDay
    
END FUNCTION