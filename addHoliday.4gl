&include "types.4gl"

FUNCTION getInitPublicHoliday(usr)
    DEFINE usr t_user
    DEFINE holiday t_publicHoliday

    LET holiday.date_holiday = TODAY
    LET holiday.description_holiday = ""
    LET holiday.id = NULL

    RETURN holiday.*
    
END FUNCTION

FUNCTION showAddHoliday(usr)
    DEFINE usr t_user
    DEFINE holiday t_publicHoliday
    DEFINE success BOOLEAN
    
    OPEN WINDOW wAddHoliday WITH FORM "addHoliday"

    CALL getInitPublicHoliday(usr.*) RETURNING holiday.*
    LET success = FALSE
    
    INPUT BY NAME holiday.*  ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED, CANCEL=FALSE)    
        ON ACTION ACCEPT
            IF validatePublicHolidayInputs(holiday.*) == TRUE THEN
                CALL sendPublicHoliday(holiday.*)
                LET success = TRUE
                EXIT INPUT
            END IF
            
        ON ACTION CLOSE
            EXIT INPUT
    
    END INPUT

    CLOSE WINDOW wAddHoliday

    RETURN success

END FUNCTION

FUNCTION sendPublicHoliday(holiday)
    DEFINE holiday t_publicHoliday

    INSERT INTO public_holidays (date_holiday, description_holiday)
    VALUES (holiday.date_holiday, holiday.description_holiday)

END FUNCTION

FUNCTION validatePublicHolidayInputs(holiday)
    DEFINE holiday t_publicHoliday
    DEFINE alreadyExists INT

-- Already exists
    SELECT COUNT(*) INTO alreadyExists FROM public_holidays WHERE date_holiday = holiday.date_holiday

    IF alreadyExists > 0 THEN
        ERROR "Holiday already exists !"
        RETURN FALSE
    END IF

    RETURN TRUE
    
END FUNCTION