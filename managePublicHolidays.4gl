&include "types.4gl"

FUNCTION showManagePublicHolidays(usr)
    DEFINE usr t_user
    DEFINE publicHolidayList DYNAMIC ARRAY OF t_publicHoliday
    DEFINE fieldyear INT
    DEFINE n om.DomNode
    
    OPEN WINDOW wManagePublicHolidays WITH FORM "managePublicHolidays"

    PREPARE reqYear FROM "SELECT YEAR(NOW())"
    EXECUTE reqYear INTO fieldyear

    LET n = getNode("Button", "buttonyear")
    CALL fillPublicHolidayList(publicHolidaylist)

    DISPLAY fieldyear TO fieldyear
    DISPLAY ARRAY publicHolidayList TO publicHoliday.*
            
        ON ACTION buttonyear
            INPUT BY NAME fieldyear
                BEFORE INPUT
                    CALL n.setAttribute("text", "Validate")
                ON ACTION buttonYear
                    CALL calculateDaysOff()
                    END INPUT
                    
        ON ACTION addHoliday
            IF showAddHoliday(usr.*) == TRUE THEN 
                CALL fillPublicHolidayList(publicHolidaylist)
            END IF

        ON ACTION removeHoliday
            IF deleteHoliday(publicHolidayList[DIALOG.getCurrentRow("publicholiday")].*) == TRUE THEN
                CALL fillPublicHolidayList(publicHolidaylist)
            END IF
        
        ON ACTION CLOSE
            EXIT DISPLAY
    END DISPLAY
    
    CLOSE WINDOW wManagePublicHolidays

END FUNCTION

FUNCTION fillPublicHolidayList(publicHolidayList)
    DEFINE publicHolidayList DYNAMIC ARRAY OF t_publicHoliday
    DEFINE i INTEGER

    CALL publicHolidayList.clear()
    
    DECLARE cPublicHolidays CURSOR FOR
        SELECT * FROM public_holidays

    LET i = 1

    FOREACH cPublicHolidays INTO publicHolidayList[i].*
        LET i = i + 1
    END FOREACH

END FUNCTION

FUNCTION deleteHoliday(publicHoliday)
    DEFINE answer STRING
    DEFINE publicHoliday t_publicHoliday

    LET answer = FGL_WINBUTTON( "Warning", "Delete holiday ?", "Lynx", "Yes|No", "question", 0)
    
    IF answer == "Yes" THEN
        DELETE FROM public_holidays WHERE id=publicHoliday.id
        RETURN TRUE
    END IF

    RETURN FALSE

END FUNCTION


FUNCTION calculateDaysOff()
END FUNCTION