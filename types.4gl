# CONNECTION FORM
SCHEMA planningdb

TYPE t_formConnect RECORD
    editUsername LIKE user.username,
    editPassword LIKE user.password
END RECORD

# USER CLASS 
TYPE t_user RECORD LIKE user.*

# HOLIDAY CLASS
TYPE t_holiday RECORD LIKE holiday.*

# PUBLIC HOLIDAYS CLASS
TYPE t_publicHoliday RECORD LIKE public_holidays.*
 
# HOLIDAY FILL TAB
TYPE t_fillHoliday RECORD
    typeH LIKE holiday.type,
    startH LIKE holiday.date_begin,
    endH LIKE holiday.date_end,
    commentH LIKE holiday.comment,
    statusH LIKE holiday.status
END RECORD
    
TYPE t_inProgressHoliday RECORD
    type LIKE holiday.type,
    date_begin LIKE holiday.date_begin,
    duration LIKE holiday.duration,
    date_end LIKE holiday.date_end,
    comment LIKE holiday.comment,
    balance_rt LIKE user.balance_rt,
    username LIKE user.username,
    id LIKE holiday.id
END RECORD

TYPE t_inProgressHolidayTmp RECORD
    type LIKE holiday.type,
    date_begin LIKE holiday.date_begin,
    duration LIKE holiday.duration,
    date_end LIKE holiday.date_end,
    comment LIKE holiday.comment,
    balance_rt LIKE user.balance_rt,
    balance_holiday LIKE user.balance_holiday,
    username LIKE user.username,
    id LIKE holiday.id
END RECORD

TYPE t_manage RECORD
    e_firstname LIKE user.firstname,
    e_lastname LIKE user.lastname,
    e_username LIKE user.username,
    e_holiday LIKE user.balance_holiday,
    e_rt LIKE user.balance_rt,
    id_chief LIKE user.id_chief,
    isAdmin LIKE user.is_admin
END RECORD

TYPE t_userPlanning RECORD
    firstname LIKE user.firstname,
    lastname LIKE user.lastname,
    id LIKE user.id
END RECORD

TYPE t_userPlanningFormated RECORD
    fullname LIKE user.firstname,
    day1 LIKE user.username,
    day2 LIKE user.username,
    day3 LIKE user.username,
    day4 LIKE user.username,
    day5 LIKE user.username,
    day6 LIKE user.username,
    day7 LIKE user.username,
    day8 LIKE user.username,
    day9 LIKE user.username,
    day10 LIKE user.username,
    day11 LIKE user.username,
    day12 LIKE user.username,
    day13 LIKE user.username,
    day14 LIKE user.username,
    day15 LIKE user.username,
    day16 LIKE user.username,
    day17 LIKE user.username,
    day18 LIKE user.username,
    day19 LIKE user.username,
    day20 LIKE user.username,
    day21 LIKE user.username,
    day22 LIKE user.username,
    day23 LIKE user.username,
    day24 LIKE user.username,
    day25 LIKE user.username,
    day26 LIKE user.username,
    day27 LIKE user.username,
    day28 LIKE user.username,
    day29 LIKE user.username,
    day30 LIKE user.username,
    day31 LIKE user.username,
    id LIKE user.id
END RECORD