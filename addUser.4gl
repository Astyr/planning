&include "types.4gl"

FUNCTION addUser(usr)
    DEFINE usr t_user
    DEFINE newUser t_user
    DEFINE answer STRING
    DEFINE userCount SMALLINT
    DEFINE logComment LIKE log_activity.comment
    OPEN WINDOW wadd WITH FORM "adduser"

    LET newUser.is_admin = FALSE
    
    INPUT BY NAME newUser.* ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED, ACCEPT=FALSE, CANCEL=FALSE)
    BEFORE INPUT
    DISPLAY "Please fill all the fields" TO infolabel
    ON ACTION submit
        IF newUser.balance_holiday >= 0 AND newUser.balance_rt >= 0 THEN
            SELECT COUNT(username) INTO userCount FROM USER
            WHERE username = newUser.username
            
            IF userCount = 0 THEN
                INSERT INTO user (firstname, lastname, is_admin, balance_holiday, balance_rt, username, password, id_chief)
                    VALUES (newUser.firstname, newUser.lastname, newUser.is_admin, newUser.balance_holiday, newUser.balance_rt, newUser.username, MD5(newUser.password), usr.id)
                LET answer = "Yes"
                LET logComment = newUser.username || " has been added"

                INSERT INTO log_activity
                (id_user, COMMENT, TYPE)
                VALUES
                (usr.id, logComment, "3")
                
                EXIT INPUT
            ELSE
                CALL FGL_WINMESSAGE( "Info", "Username already used !", "exclamation")
            END IF

        ELSE
            CALL FGL_WINMESSAGE( "Info", "Balances must be positive !", "exclamation")
        END IF

    ON ACTION CLOSE
        LET answer = "No"
        EXIT INPUT
        
    END INPUT
    CLOSE WINDOW wadd
    RETURN answer
END FUNCTION