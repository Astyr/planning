&include "types.4gl"

FUNCTION showLog()
    DEFINE allLog DYNAMIC ARRAY OF RECORD
        id LIKE log_activity.id,
        username LIKE user.username,
        COMMENT LIKE log_activity.comment,
        TYPE LIKE log_activity.type
        END RECORD
    DEFINE i INT 
    DEFINE tmp RECORD 
        id LIKE log_activity.id,
        username LIKE user.username,
        COMMENT LIKE log_activity.comment,
        TYPE LIKE log_activity.type
        END RECORD
    OPEN WINDOW w1 WITH FORM "showLog"

 
    
    DECLARE curs_list CURSOR FOR
    SELECT log_activity.id, username, COMMENT, TYPE
    FROM log_activity, USER
    WHERE user.id = log_activity.id_user

    FOREACH curs_list INTO tmp.*
    LET i = i + 1
    LET allLog[i].* = tmp.*
    END FOREACH
    
    DISPLAY ARRAY allLog TO record1.*
        ON ACTION CLOSE
            EXIT DISPLAY
    END DISPLAY
    CLOSE WINDOW w1
END FUNCTION