&include "types.4gl"

FUNCTION getInitHoliday(usr)
    DEFINE usr t_user
    DEFINE holiday t_holiday

    LET holiday.comment = "None."
    LET holiday.date_begin = TODAY
    LET holiday.date_end = TODAY
    LET holiday.duration = 2
    LET holiday.id = NULL
    LET holiday.id_user = usr.id
    LET holiday.is_allday = "0"
    LET holiday.is_begin_halfday = FALSE
    LET holiday.is_end_halfday = FALSE
    LET holiday.status = "Waiting"
    LET holiday.type = "Holidays"

    RETURN holiday.*
    
END FUNCTION

FUNCTION showRegisterHoliday(usr)
    DEFINE usr t_user
    DEFINE holiday t_holiday
    DEFINE logComment LIKE log_activity.comment
    
    OPEN WINDOW wRegisterHoliday WITH FORM "registerHoliday"
  
    CALL getInitHoliday(usr.*) RETURNING holiday.*

    INPUT BY NAME holiday.*  ATTRIBUTES(WITHOUT DEFAULTS, UNBUFFERED, CANCEL=FALSE)
        BEFORE INPUT
            CALL displayDateFields(holiday.*)
            CALL colorDate(holiday.date_begin, "begin")
            CALL colorDate(holiday.date_begin, "end")
            LET holiday.duration = calculateDuration(holiday.*, usr.*)
            DISPLAY usr.balance_holiday / 2 TO balanceHolidays
            DISPLAY usr.balance_rt / 2 TO balanceDaysoff
    
        ON CHANGE type
            CALL displayDateFields(holiday.*)
            LET holiday.duration = calculateDuration(holiday.*, usr.*)
            
        ON CHANGE date_begin
            CALL displayDateFields(holiday.*)
            CALL colorDate(holiday.date_begin, "begin")
            LET holiday.duration = calculateDuration(holiday.*, usr.*)
            
        ON CHANGE date_end
            CALL displayDateFields(holiday.*)
            CALL colorDate(holiday.date_end, "end")
            LET holiday.duration = calculateDuration(holiday.*, usr.*)
            
        ON CHANGE is_begin_halfday
            LET holiday.duration = calculateDuration(holiday.*, usr.*)
            
        ON CHANGE is_end_halfday
            LET holiday.duration = calculateDuration(holiday.*, usr.*)
            
        ON CHANGE is_allday
            LET holiday.duration = calculateDuration(holiday.*, usr.*)

        ON ACTION ACCEPT
            IF validateRegisterInputs(holiday.*) == TRUE THEN
                CALL sendHoliday(holiday.*)
         
                LET logComment = "Register holiday from " || holiday.date_begin || " to " || holiday.date_end       
                INSERT INTO log_activity
                (id_user, COMMENT, TYPE)
                VALUES
                (usr.id, logComment, "1")
                EXIT INPUT
            END IF
            
        ON ACTION CLOSE
            EXIT INPUT
    
    END INPUT

    CLOSE WINDOW wRegisterHoliday
    
END FUNCTION

FUNCTION validateRegisterInputs(holiday)
    DEFINE holiday t_holiday

-- Start after end
    IF holiday.duration <= 0 THEN
        ERROR "Duration invalid !"
        RETURN FALSE
    END IF

-- Not a working day
    IF execIsWorkingDay(holiday.date_begin) == FALSE THEN
        ERROR "Begin date is not a working day !"
        RETURN FALSE
    END IF

-- Not a working day
    IF execIsWorkingDay(holiday.date_end) == FALSE THEN
        ERROR "End date is not a working day !"
        RETURN FALSE
    END IF

    RETURN TRUE
    
END FUNCTION

FUNCTION colorDuration(duration)
    DEFINE duration INTEGER
    DEFINE n om.DomNode
    DEFINE color STRING

    LET n = getFormField("formonly.durationindays")

    IF duration > 0 THEN
        LET color = "green"
    ELSE
        LET color = "red"
    END IF

    CALL n.setAttribute("color", color)
    
END FUNCTION

FUNCTION colorDate(dateValue, toColor)
    DEFINE toColor, color STRING
    DEFINE dateValue DATE
    DEFINE n om.DomNode

    LET n = NULL
    
    CASE toColor
        WHEN "begin"
            LET n = getFormField("formonly.date_begin")
        WHEN "end"
            LET n = getFormField("formonly.date_end")
    END CASE

    IF n IS NOT NULL THEN
        IF execIsWorkingDay(dateValue) == TRUE THEN
            LET color = "green"
        ELSE
            LET color = "red"
        END IF

        CALL n.setAttribute("color", color)
    END IF
            
END FUNCTION
    
FUNCTION sendHoliday(holiday)
    DEFINE holiday t_holiday

    INSERT INTO holiday (comment, date_begin, date_end, duration, id_user, is_allday, is_begin_halfday, is_end_halfday, status, type)
    VALUES (holiday.comment, holiday.date_begin, holiday.date_end, holiday.duration, holiday.id_user, holiday.is_allday, holiday.is_begin_halfday, holiday.is_end_halfday, holiday.status, holiday.type)
    
END FUNCTION

FUNCTION calculateDuration(holiday, usr)
    DEFINE holiday t_holiday
    DEFINE usr t_user
    DEFINE duration INTEGER

    IF execIsWorkingDay(holiday.date_begin) == FALSE ||
    execIsWorkingDay(holiday.date_end) == FALSE THEN
        LET duration = 0
    ELSE
        -- *2 Because duration is calculated in half days 
        LET duration = execNbWorkingDays(holiday.date_begin, holiday.date_end) * 2
    
        IF holiday.type != "Days off" THEN
            IF holiday.date_begin == holiday.date_end THEN
                LET duration = IIF(holiday.is_allday == "0", 2, 1)
            ELSE
                LET duration = duration - IIF(holiday.is_begin_halfday == TRUE, 1, 0)
                LET duration = duration - IIF(holiday.is_end_halfday == TRUE, 1, 0)
            END IF
        END IF
    END IF

    IF duration < 0 THEN
        LET duration = 0
    END IF

    CALL colorDuration(duration)
    CALL updatePlannedRemaining(usr.*, holiday.type, duration)
    
    RETURN duration

END FUNCTION

FUNCTION updatePlannedRemaining(usr, holiday_type, duration)
    DEFINE usr t_user
    DEFINE holiday_type STRING
    DEFINE duration INT
    DEFINE pH, pD INT

    LET pH = usr.balance_holiday
    LET pD = usr.balance_rt
    
    CASE holiday_type
        WHEN "Holidays"
            LET pH = pH - duration
        WHEN "Days off"
            LET pD = pD - duration
    END CASE

    DISPLAY pH / 2 TO plannedRemainingHolidays
    DISPLAY pD / 2 TO plannedRemainingDaysoff
    DISPLAY duration / 2 TO durationInDays

END FUNCTION

FUNCTION displayDateFields(holiday)
    DEFINE holiday t_holiday
    DEFINE win ui.Window
    DEFINE f ui.Form

    LET win = ui.Window.getCurrent()
    LET f = win.getForm()

    IF holiday.type == "Days off" THEN
        CALL f.setFieldHidden("formonly.is_allday", TRUE)
        CALL f.setFieldHidden("formonly.is_begin_halfday", TRUE)
        CALL f.setFieldHidden("formonly.is_end_halfday", TRUE)
    ELSE
        IF holiday.date_begin == holiday.date_end THEN
            CALL f.setFieldHidden("formonly.is_allday", FALSE)
            CALL f.setFieldHidden("formonly.is_begin_halfday", TRUE)
            CALL f.setFieldHidden("formonly.is_end_halfday", TRUE)
        ELSE
            CALL f.setFieldHidden("formonly.is_allday", TRUE)
            CALL f.setFieldHidden("formonly.is_begin_halfday", FALSE)
            CALL f.setFieldHidden("formonly.is_end_halfday", FALSE)
        END IF
    END IF

END FUNCTION